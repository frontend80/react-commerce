export default function Button({ text, link }) {
  return (
    <div className="rounded-md shadow mr-6">
      <a href={link} className="w-full flex items-center justify-center px-6 py-2 border border-mpp-orange text-base font-medium rounded-md text-white bg-mpp-orange hover:bg-mpp-orange md:text-lg">
        {text}
      </a>
    </div>
  )
}
