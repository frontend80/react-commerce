export default function Title({ title, color, uppercase }) {
  console.log('uppercase', uppercase)
  return (
    <h1 className={`text-2xl ${uppercase && 'uppercase'} font-sans tracking-tight font-normal text-${color} sm:text-3xl md:text-4xl lg:text-3xl xl:text-4xl`}>
      <span className="block xl:inline">{title}</span>
    </h1>
  )
}
