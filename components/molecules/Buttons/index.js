import Button from '../../atoms/Button'

export default function Buttons({ buttons, position }) {
  console.log('position', position)
  return (
    <div className={`mt-10 w-full sm:flex sm:justify-center lg:${position === 'center' ? 'justify-center' : 'justify-start'}`}>
      {buttons.map(button => (
        <Button key={button.text} {...button} />
      ))}
    </div>
  )
}