import Container from '../../atoms/Container'
import Title from '../../atoms/Title'

// Naming
// Text + image on the side

export default function Slice2({content, style}) {
  const color = 'text-gray-500'
  return (
    <section className="relative w-full py-10 overflow-hidden">
      <Container>
        <Title title={content.title} color="gray-900" />
        <div className="lg:grid lg:grid-cols-12 lg:gap-8">
          <div className="sm:text-center md:max-w-2xl md:mx-auto lg:col-span-6 lg:text-left">
            <div className="mt-3 ml-6 text-base text-gray-800 sm:mt-12" dangerouslySetInnerHTML={{ __html: content.text }}>
            </div>
          </div>
          <div className="relative mt-12 sm:max-w-lg sm:mx-auto lg:mt-0 lg:max-w-none lg:mx-0 lg:col-span-6 lg:flex lg:items-center">
            {/* <svg class="absolute top-0 left-1/2 transform -translate-x-1/2 -translate-y-8 scale-75 origin-top sm:scale-100 lg:hidden" width="640" height="784" fill="none" viewBox="0 0 640 784" aria-hidden="true">
              <defs>
                <pattern id="4f4f415c-a0e9-44c2-9601-6ded5a34a13e" x="118" y="0" width="20" height="20" patternUnits="userSpaceOnUse">
                  <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor" />
                </pattern>
              </defs>
              <rect y="72" width="640" height="640" class="text-gray-50" fill="currentColor" />
              <rect x="118" width="404" height="784" fill="url(#4f4f415c-a0e9-44c2-9601-6ded5a34a13e)" />
            </svg> */}
            <div className="relative w-full mx-auto lg:max-w-md">
              <img className="w-full" src={content.image.src} alt="" />
              {/* <button type="button" class="relative block w-full bg-white rounded-lg overflow-hidden focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <span class="sr-only">Watch our video to learn more</span>
                <img class="w-full" src={content.image} alt="" />
              </button> */}
            </div>
          </div>
        </div>
      </Container>
      {/* <div className="relative w-full h-64 sm:h-72 md:h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-2/5 lg:h-full">
        <img className="absolute inset-0 object-cover w-full h-full" src={content.image} alt="" />
      </div> */}
    </section>
  )
}
