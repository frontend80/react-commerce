import Container from '../../atoms/Container'
import Title from '../../atoms/Title'

// Naming
// Text + image on the side

export default function Slice3({content, style}) {
  const color = 'text-gray-500'
  return (
    <section className={`relative w-full py-10 overflow-hidden bg-mpp-${style.background.color}`}>
      <Container>
        <Title title={content.title} color="gray-900" />
        <div className="grid grid-cols-12 gap-8 mt-12">
          {content.args && content.args.map(arg => (
            <div key={arg.title} className="col-span-12 mx-auto text-center md:col-span-6 lg:col-span-3">
              <div className="flex justify-center w-full">
                <img className="h-20" src={arg.image.src} alt="Tuple" />
              </div>
              <div className="font-bold">{arg.title}</div>
              <div className="">{arg.text}</div>
            </div>
          ))}
        </div>
      </Container>
      {/* <div className="relative w-full h-64 sm:h-72 md:h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-2/5 lg:h-full">
        <img className="absolute inset-0 object-cover w-full h-full" src={content.image} alt="" />
      </div> */}
    </section>
  )
}
