import Container from '../../atoms/Container'
import Title from '../../atoms/Title'

// Naming
// Text + image on the side

export default function Slice4({content, style}) {
  const color = 'text-gray-500'
  return (
    <section className={`relative w-full py-10 overflow-hidden`}>
      <Container>
        <Title title={content.title} color="gray-900" />
        <div className="grid grid-cols-12 gap-8 mt-12">
          {content.categories && content.categories.map(cat => (
            <div class="col-span-12 md:col-span-6 lg:col-span-4 flex items-start justify-between w-full">
              <div class="mr-3">
                <div className="text-mpp-blue">{cat.title}</div>
                <div className="text-sm">
                  {cat.products && cat.products.map(product => (
                    <div>{product.title}</div>
                  ))}
                </div>
                {cat.button && (
                  <div className={`w-full mt-3 flex justify-start`}>
                    <div className="rounded-md shadow">
                      <a href={cat.button.link} className="flex items-center justify-center w-full px-3 py-0 text-base font-medium text-white border rounded-md border-mpp-blue bg-mpp-blue hover:bg-mpp-blue md:text-lg">
                        {cat.button.text}
                      </a>
                    </div>
                  </div>
                )}
              </div>
              <div class="">
                <a href={cat.button.link}>
                  <img class="h-44 w-44" src={cat.image.src} alt="" />
                </a>
              </div>
            </div>
          ))}
        </div>
      </Container>
      {/* <div className="relative w-full h-64 sm:h-72 md:h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-2/5 lg:h-full">
        <img className="absolute inset-0 object-cover w-full h-full" src={content.image} alt="" />
      </div> */}
    </section>
  )
}
