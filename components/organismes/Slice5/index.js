import Container from '../../atoms/Container'
import Title from '../../atoms/Title'
import Buttons from '../../molecules/Buttons'

// Naming
// Text + image on the side

export default function Slice5({content, style}) {
  const color = 'text-gray-500'
  return (
    <section className={`w-full relative py-10 overflow-hidden bg-mpp-${style.background.color}`}>
      <Container>
        <Title title={content.title} color="gray-900" />
        <div className="lg:grid lg:grid-cols-12 lg:gap-8">
          <div className="sm:text-center md:max-w-2xl md:mx-auto lg:col-span-6 lg:text-left">
            <div className="relative w-full mx-auto lg:max-w-md">
              <img className="w-full" src={content.image.src} alt="" />
            </div>
          </div>
          <div className="relative mt-12 sm:max-w-lg sm:mx-auto lg:mt-0 lg:max-w-none lg:mx-0 lg:col-span-6 lg:flex lg:items-center">
            <div className="flex flex-wrap">
              {content.args && content.args.map(arg => (
                <div key={arg.text} className="flex w-full mb-8 sm:w-1/2">
                  <div className="mr-2">
                    <img src={content.icon} alt="" />
                  </div>
                  <div>{arg.text}</div>
                </div>
              ))}
              {content.buttons && (
                <Buttons buttons={content.buttons} position="center" />
              )}
            </div>
          </div>
        </div>
      </Container>
    </section>
  )
}
