import Container from '../../atoms/Container'
import Title from '../../atoms/Title'
import Buttons from '../../molecules/Buttons'

// Naming
// Text + image on the side

export default function Slice6({content, style}) {
  const color = 'text-gray-500'
  return (
    <section className="relative w-full py-10 overflow-hidden">
      <Container>
        <Title title={content.title} color="gray-900" />
        <div className="lg:grid lg:grid-cols-12 lg:gap-8">
          <div className="sm:text-center md:max-w-2xl md:mx-auto lg:col-span-6 lg:text-left">
            <div className="mt-3 text-base text-gray-800 sm:mt-12" dangerouslySetInnerHTML={{ __html: content.text }}>
            </div>
            {content.buttons && (
              <Buttons buttons={content.buttons} />
            )}
          </div>
          <div className="relative mt-12 sm:max-w-lg sm:mx-auto lg:mt-0 lg:max-w-none lg:mx-0 lg:col-span-6 lg:flex lg:items-center">
            <div className="relative w-full mx-auto lg:max-w-md">
              {/* <img class="w-full" src={content.image} alt="" /> */}
              {/* <button type="button" class="relative block w-full bg-white rounded-lg overflow-hidden focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <span class="sr-only">Watch our video to learn more</span>
                <img class="w-full" src={content.image} alt="" />
              </button> */}
            </div>
          </div>
        </div>
      </Container>
      {/* <div className="relative w-full h-64 sm:h-72 md:h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-2/5 lg:h-full">
        <img className="absolute inset-0 object-cover w-full h-full" src={content.image} alt="" />
      </div> */}
    </section>
  )
}
