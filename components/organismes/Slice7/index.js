import Container from '../../atoms/Container'
import Title from '../../atoms/Title'

// Naming
// Text + BG image on the side

export default function Slice7({content, style}) {
  const color = 'text-gray-500'
  return (
    <section className="w-full relative bg-mpp-blue py-10 overflow-hidden">
      <Container>
        <div className="w-full text-center lg:text-left">
          <div className="px-4 lg:w-3/5 sm:px-8 xl:pr-16">
            <Title title={content.title} color='white' uppercase />
            <div className="mt-3 max-w-md mx-auto font-normal text-base text-gray-200 md:mt-5 md:max-w-3xl" dangerouslySetInnerHTML={{ __html: content.text }}>
              {/* Prismic Rich Text */}
              {/* {content.text} */}
            </div>
            {/* <div className="mt-10 sm:flex sm:justify-center lg:justify-start">
              <div className="rounded-md shadow">
                <a href="#" className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 md:py-4 md:text-lg md:px-10">
                  Get started
                </a>
              </div>
              <div className="mt-3 rounded-md shadow sm:mt-0 sm:ml-3">
                <a href="#" className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-gray-50 md:py-4 md:text-lg md:px-10">
                  Live demo
                </a>
              </div>
            </div> */}
          </div>
        </div>
      </Container>
      <div className="relative w-full h-64 sm:h-72 md:h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-2/5 lg:h-full">
        <img className="absolute inset-0 w-full h-full object-cover" src={content.image.src} alt="" />
      </div>
    </section>
  )
}
