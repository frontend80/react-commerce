export default {
  content: {
    title: 'Your super intendent pharmacist',
    text: `
      <p><b>Mr. Babak Gholamhosseini</b></p>
      <p>GPhC registration no. 2084131</p>
      <p>MRPharmS</p><br/>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui. Duis dignissim auctor mauris a ornare. Mauris nunc lacus, dignissim sed feugiat eleifend, dignissim ac felis. Phasellus et urna sed nibh interdum ultrices. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
    `,
    image: {
      src: './__mocks__/about/image_profile.png'
    }
  },
  style: {
    divide: 3
  }
}