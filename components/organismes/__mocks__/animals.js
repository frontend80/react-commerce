export default {
  content: {
    title: 'Animal Prescriptions',
    icon: './__mocks__/animals/check.png',
    args: [
      {
        text: 'Genuine Uk medicines',
      },
      {
        text: 'Same day dispatch',
      },
      {
        text: 'Best practice garantee',
      },
      {
        text: 'Free delivery',
      }
    ],
    image: {
      src: './__mocks__/animals/animals.png'
    },
    buttons: [
      {
        text: 'Dispense now',
        link: '/'
      }
    ]
  },
  style: {
    divide: 3,
    background: {
      color: 'blue_light',
      // image: {
      //   src: './__mocks__/animals/animals.png'
      // }
    },
  }
}