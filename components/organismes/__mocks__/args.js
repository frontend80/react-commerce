export default {
  content: {
    title: 'Here is why everybody loves My Private Pharmacist',
    args: [
      {
        title: 'Save time',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: {
          src: './__mocks__/args/img1.png'
        }
      },
      {
        title: 'Free consultation',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: {
          src: './__mocks__/args/img2.png'
        }
      },
      {
        title: 'Trusted Pharmacy',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: {
          src: './__mocks__/args/img3.png'
        }
      },
      {
        title: 'Free delivery',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        image: {
          src: './__mocks__/args/img4.png'
        }
      },
    ]
  },
  style: {
    divide: 3,
    background: {
      color: 'blue_light',
    },
  }
}