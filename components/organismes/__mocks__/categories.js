export default {
  content: {
    title: 'Treatments from My Private Pharmacist',
    categories: [
      {
        title: 'Mens Health',
        products: [
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          }
        ],
        button: {
          text: 'View all',
          link: '/'
        },
        image: {
          src: './__mocks__/categories/img1.png'
        }
      },
      {
        title: 'Womens Health',
        products: [
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          }
        ],
        button: {
          text: 'View all',
          link: '/'
        },
        image: {
          src: './__mocks__/categories/img2.png'
        }
      },
      {
        title: 'General Treatments',
        products: [
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          }
        ],
        button: {
          text: 'View all',
          link: '/'
        },
        image: {
          src: './__mocks__/categories/img3.png'
        }
      },
      {
        title: 'Travel Health',
        products: [
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          }
        ],
        button: {
          text: 'View all',
          link: '/'
        },
        image: {
          src: './__mocks__/categories/img4.png'
        }
      },
      {
        title: 'Health & Beauty',
        products: [
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          },
          {
            title: 'Lorem Ipsum',
            link: '/',
          }
        ],
        button: {
          text: 'View all',
          link: '/'
        },
        image: {
          src: './__mocks__/categories/img5.png'
        }
      },
    ]
  },
  style: {
    divide: 3,
  }
}