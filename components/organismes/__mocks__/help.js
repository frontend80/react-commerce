export default {
  content: {
    title: 'Need help with selecting the right treatment or advice on safety of your medicine',
    text: `
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui. Duis dignissim auctor mauris a ornare. Mauris nunc lacus, dignissim sed feugiat eleifend, dignissim ac felis.</p><br/>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui. </p><br/>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui. Duis dignissim auctor mauris a ornare.</p>
    `,
    actions: [
      {
        text: 'Video Chat',
        link: '/',
        image: {
          src: './__mocks__/how/how.png'
        }
      },
      {
        text: 'Phone',
        link: '/',
        image: {
          src: './__mocks__/how/how.png'
        }
      },
      {
        text: 'Live Chat',
        link: '/',
        image: {
          src: './__mocks__/how/how.png'
        }
      },
      {
        text: 'E-mail',
        link: '/',
        image: {
          src: './__mocks__/how/how.png'
        }
      }
    ],
    buttons: [
      {
        text: 'Speack to My Private Pharmacist',
        link: '/'
      }
    ]
  },
  style: {
    divide: 3
  }
}