export default {
  content: {
    title: 'Helpful. Trustworthy. Personal medicines & more from My Private Pharmacist.',
    text: `
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui.</p>
    `,
    logos: [
      {
        src: './__mocks__/global/registered.png',
        alt: 'Registered Number',
        height: 10,
      },
      {
        src: './__mocks__/global/trustpilot.png',
        alt: 'Trustpilot rank',
        height: 6,
      }
    ],
    buttons: [
      {
        text: 'Treatments',
        link: '/'
      },
      {
        text: 'Prescriptions',
        link: '/'
      }
    ]
  },
  style: {
    divide: 3,
    background: {
      // color: 'blue_light',
      image: {
        src: './__mocks__/hero/bg_hero.png'
      },
      image_mobile: {
        src: './__mocks__/hero/bg_hero_mobile.png'
      }
    },
  }
}