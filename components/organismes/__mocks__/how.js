export default {
  content: {
    title: 'How it work ?',
    text: `
      <ol class="list-decimal">
        <li>
          <p><b>Select your treatment</b></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui.</p>
          <br/>
        </li>
        <li>
          <p><b>Select your treatment</b></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui.</p>
          <br/>
        </li>
        <li>
          <p><b>Select your treatment</b></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui.</p>
          <br/>
        </li>
        <li>
          <p><b>Select your treatment</b></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mi mi, laoreet et metus rutrum, scelerisque lobortis dui.</p>
          <br/>
        </li>
      </ol>
    `,
    image: {
      src: './__mocks__/how/how.png'
    }
  },
  style: {
    divide: 3
  }
}