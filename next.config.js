const nextConfig = {
  i18n: {
    localeDetection: false,
    locales: ['en-gb'],
    defaultLocale: 'en-gb',
  }
}

module.exports = nextConfig