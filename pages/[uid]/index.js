import React from 'react'
import { getAllPaths, getDocByUid } from '../../sdk'

const Page = ({ title, slices }) => {
  console.log('title', title)
  // console.log('slices', slices)
  return (
    <>
      <div>Title: { title }</div>
      {slices.map(slice => (
        <div key={slice.type}>Slice: {slice.type}</div>
      ))}
    </>
  )
}


export async function getStaticProps({params, locale, preview = false, previewData}) {
  const doc = await getDocByUid({
    env: process.env,
    customType: 'Page',
    uid: params.uid,
    locale,
    previewData
  })
  return {
    props: {
      ...doc,
    },
    revalidate: 1,
  }
}

export async function getStaticPaths(ctx) {
  const paths = await getAllPaths({
    env: process.env,
    ...ctx,
    customType: 'Page'
  })
  return {
    paths,
    fallback: false,
  }
}

export default Page