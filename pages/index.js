import Head from 'next/head'
import Slice1 from '../components/organismes/Slice1'
import Slice2 from '../components/organismes/Slice2'
import Slice3 from '../components/organismes/Slice3'
import Slice4 from '../components/organismes/Slice4'
import Slice5 from '../components/organismes/Slice5'
import Slice6 from '../components/organismes/Slice6'
import Slice7 from '../components/organismes/Slice7'

import hero from '../components/organismes/__mocks__/hero'
import how from '../components/organismes/__mocks__/how'
import categories from '../components/organismes/__mocks__/categories'
import args from '../components/organismes/__mocks__/args'
import animals from '../components/organismes/__mocks__/animals'
import help from '../components/organismes/__mocks__/help'
import about from '../components/organismes/__mocks__/about'

import { getAllPaths } from '../sdk'

export default function Home() {
  return (
    <div className="flex flex-col justify-center">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Slice1 {...hero}/>
      <Slice2 {...how}/>
      <Slice3 {...args}/>
      <Slice4 {...categories}/>
      <Slice5 {...animals}/>
      <Slice6 {...help}/>
      <Slice7 {...about}/>
    </div>
  )
}

export async function getStaticProps(ctx) {
  const allPaths = await getAllPaths({
    env: process.env,
    ...ctx,
    customType: 'Page'
  })
  return {
    props: { 
      pages: { ...allPaths },
      ctx: { ...ctx }
    } 
  }
}

