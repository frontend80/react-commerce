import Prismic from '@prismicio/client'

// TODO: mettre les variables dans un fichier env
// Mettre le client Prismic dans un hook HTTP avec config Prismic

export const apiEndpoint = 'https://myprivatepharmacist.cdn.prismic.io/api/v2'
export const accessToken = 'MC5ZQlJ1QmhVQUFDTUFiS0FV.77-9cV_vv71877-9HO-_vSBE77-9au-_vVbvv73vv71r77-977-9Ju-_vR_vv73vv73vv73vv70p77-9Qu-_vRAz'

// Client method to query documents from the Prismic repo
export const Client = (req = null) => (
  Prismic.client(apiEndpoint, createClientOptions(req, accessToken))
)

const createClientOptions = (req = null, prismicAccessToken = null) => {
  const reqOption = req ? { req } : {}
  const accessTokenOption = prismicAccessToken ? { accessToken: prismicAccessToken } : {}
  return {
    ...reqOption,
    ...accessTokenOption,
  }
}
