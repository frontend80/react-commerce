import { getAllUid_Prismic, getDocByUid_Prismic } from './prismic'

export async function getAllUid({ env, customType }) {
  let allUid = []
  switch (env.NEXT_STATIC_CMS_API) {
    case 'Prismic':
      allUid = await getAllUid_Prismic({env, customType})
      break;
    default:
      console.log(`Unknown Http Client ${expr}.`);
  }
  return allUid
}

export const getAllPaths = async ({ env, defaultLocale, customType }) => {
  const allUid = await getAllUid({ env, customType })
  const allPaths = allUid.map(path => {
    if(path.locale === defaultLocale) {
      return `/${path.uid}`
    }
    return `/${path.locale}/${path.uid}`
  })
  return allPaths
}

export const getDocByUid = async ({env, customType, uid, locale, previewData}) => {
  let doc = {}
  switch (env.NEXT_STATIC_CMS_API) {
    case 'Prismic':
      doc = await getDocByUid_Prismic({env, customType, uid, locale, previewData})
      break;
    default:
      console.log(`Unknown Http Client ${expr}.`);
  }
  return doc
}