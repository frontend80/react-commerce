import Prismic from 'prismic-javascript'

export const gethttpClient_Prismic = ({ accessToken, repository }) => {
  const REF_API_URL = `https://${repository}.prismic.io/api/v2`
  const GRAPHQL_API_URL = `https://${repository}.prismic.io/graphql`
  return Prismic.client(REF_API_URL, {
    accessToken,
    graphQlApiUrl: GRAPHQL_API_URL,
  })
}

async function fetchAPI(env, query, { previewData, variables } = {}) {
  const PrismicClient = await gethttpClient_Prismic({
    accessToken: env.NEXT_SERVER_PRISMIC_API_TOKEN,
    repository: env.NEXT_STATIC_PRISMIC_REPOSITORY_NAME
  })
  const prismicAPI = await PrismicClient.getApi()
  const res = await fetch(
    `${prismicAPI.options.graphQlApiUrl}?query=${query}&variables=${JSON.stringify(variables)}`,
    {
      headers: {
        'Prismic-Ref': previewData?.ref || prismicAPI.masterRef.ref,
        'Content-Type': 'application/json',
        Authorization: `Token ${prismicAPI.options.accessToken}`,
      },
    }
  )

  if (res.status !== 200) {
    console.log(await res.text())
    throw new Error('Failed to fetch API')
  }

  const json = await res.json()
  if (json.errors) {
    console.error(json.errors)
    throw new Error('Failed to fetch API')
  }
  return json.data
}

export async function getAllUid_Prismic({env, customType}) {
  const response = await fetchAPI(env, `
    {
      all${customType}s {
        edges {
          node {
            _meta {
              uid
              lang
            }
          }
        }
      }
    }
  `)

  console.log('data from getAllUid', response)
  return response[`all${customType}s`].edges.map(doc => {
    return {
      uid: doc.node._meta.uid,
      locale: doc.node._meta.lang
    }
  })
}

export async function getDocByUid_Prismic({env, customType, uid, locale, previewData}) {

  const response = await fetchAPI(env, `
  query DocByUid($uid: String!, $locale: String!) {
    ${customType.toLowerCase()}(uid: $uid, lang: $locale) {
      title
      _meta {
        uid
      }
    }
  }
  `,
  {
    previewData,
    variables: {
      uid,
      locale,
    },
  })

  const sliceTypes = await getSliceTypes_Prismic({env, customType, uid, locale, previewData})

  // const sliceTypes = response.page.body.map(slice => slice.__typename)
  // console.log('sliceTypes', sliceTypes)

  const flatSlice = []
  const slices = await Promise.all(sliceTypes.map(async (sliceType) => {
    if(sliceType.split('Body').reverse()[0] === 'Persistent_slice'){
      const persistentSlice = await getSlice_Prismic({env, customType, sliceType, uid, locale, previewData})
      const persistentSliceUid = persistentSlice.primary.persistent_slice._meta.uid
      // console.log('persistentSliceUid', persistentSliceUid)
      const persistentSliceTypes = await getSliceTypes_Prismic({env, customType: 'Slice', uid: persistentSliceUid, locale, previewData})
      // console.log('persistentSliceTypes', persistentSliceTypes)
      const persistentSliceGroup = await Promise.all(persistentSliceTypes.map(async (persistentSliceType) => {
        // console.log('persistentSliceType', persistentSliceType.split('Body').reverse()[0])
        const slice = await getSlice_Prismic({env, customType: 'Slice', sliceType: persistentSliceType, uid: persistentSliceUid, locale, previewData})
        flatSlice.push(slice)
        return slice
      }))
      // console.log('persistentSliceGroup', persistentSliceGroup)
      
      return persistentSliceGroup
    }
    const slice = await getSlice_Prismic({env, customType, sliceType, uid, locale, previewData})
    flatSlice.push(slice)
    return slice
  }))

  // console.log('flatSlice', flatSlice)

  return {
    title: response.page.title,
    slices: flatSlice
  }
}

export async function getSliceTypes_Prismic({env, customType, uid, locale, previewData}) {
  const response = await fetchAPI(env, `
  query DocByUid($uid: String!, $locale: String!) {
    ${customType.toLowerCase()}(uid: $uid, lang: $locale) {
      body{
        __typename
      }
    }
  }
  `,
  {
    previewData,
    variables: {
      uid,
      locale,
    },
  })

  return response[customType.toLowerCase()].body.map(slice => slice.__typename)
}

export async function getSlice_Prismic({env, customType, sliceType, uid, locale, previewData}) {
  const query = queries.find(query => query.key === sliceType.split('Body').reverse()[0]).query

  // console.log('getSlice_Prismic sliceType', sliceType)
  // console.log('getSlice_Prismic query', query)
  // console.log('getSlice_Prismic uid', uid)

  const response = await fetchAPI(env, `
    query Slice($uid: String!, $locale: String!) {
      ${customType.toLowerCase()}(uid: $uid, lang: $locale) {
        body{
          ... on ${sliceType}{
            ${query}
          }
        }
      }
    }
  `,
  {
    previewData,
    variables: {
      uid,
      locale,
    },
  })

  // console.log('response', response)

  return response[customType.toLowerCase()].body.find(slice => slice.__typename === sliceType)
}

export async function getSlices_Prismic({env, customType, sliceTypes, uid, locale, previewData}) {
  let sliceQueries = ``

  for (let i = 0; i < sliceTypes.length; i++) {
    const query = queries.find(query => query.key === sliceTypes[i].split('Body').reverse()[0]).query
    sliceQueries += `
      ... on ${sliceTypes[i]}{
        ${query}
      }
    `
  }

  const response = await fetchAPI(env, `
  query Slice($uid: String!, $locale: String!) {
    ${customType.toLowerCase()}(uid: $uid, lang: $locale) {
      body{
        ${sliceQueries}
      }
    }
  }
  `,
  {
    previewData,
    variables: {
      uid,
      locale,
    },
  })

  // console.log('response', response.page.body)

  // const sliceTypes = response.page.body
  // console.log('sliceTypes', sliceTypes)

  // // Savoir comment récupérer les infos de la slice
  // // En first, il faut savoir comment interroger la slice ? Récupérer la structure
  // // En second, il faut refaire une requête pour aller chercher de l'info sur la slice
  // // en sachant quel uid taper

  // return {
  //   title: response.page.title
  // }
  return response.page.body
}

const queries = [
  {
    key: 'Hello_world_dynamic',
    query: `
      __typename
      type
      primary {
        text
      }
    `
  },
  {
    key: 'Hello_world_persistent',
    query: `
      __typename
      type
      primary {
        text
      }
    `
  },
  {
    key: 'Persistent_slice',
    query: `
      __typename
      type
      primary{
        persistent_slice{
          ... on Slice{
            slice_title
            body{
              __typename
            }
            _meta{
              uid
              lang
            }
          }
        }
      }
    `
  }
]

// const query= `
// type
// primary {
//   text
// }
// `