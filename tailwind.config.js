module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'sans': 'Roboto, ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
      'serif': 'Roboto, ui-serif, Georgia, Cambria, "Times New Roman", Times, serif',
    },
    extend: {
      // backgroundImage: theme => ({
      //   'hero-pattern': "url('/img/hero-pattern.svg')",
      // }),
      colors: {
        'mpp-blue': '#1c5fb4',
        'mpp-blue_light': '#e9f4f8',
        'mpp-orange': '#f4951a',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
